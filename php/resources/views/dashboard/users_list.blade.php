@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-9 col-sm-11">
                <h1 class="">{{$title ?? ''}}</h1>

                <div class="card">
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if($users != null && count($users) > 0)
                            <ul class="list-group">
                                @foreach($users as $user)
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        <p class="p-0 m-0 flex-grow-1">{{$user->name}}</p>
                                        <div>
                                            @if($user->role->id == 1)
                                                <span class="badge badge-primary badge-pill mr-4">Stranka</span>
                                            @elseif($user->role->id == 2)
                                                <span class="badge badge-warning badge-pill mr-4">Prodajalec</span>
                                            @elseif($user->role->id == 3)
                                                <span class="badge badge-danger badge-pill mr-4">Admin</span>
                                            @endif
                                            <a role="button" class="btn btn-sm btn-success pull-right" href="/dashboard/{{$user->id}}">Uredi podatke</a>
                                            @if($user->active == true)
                                                {!! Form::open(['action' => ['ProfileController@toggleActivity'], 'method' => 'POST', 'class' => 'float-right pl-1']) !!}
                                                    {{ Form::hidden('action', 'deactivate')}}
                                                    {{ Form::hidden('user_id', $user->id)}}
                                                    {{ Form::submit('Deaktiviraj', ['class' => 'btn btn-sm btn-danger'])}}
                                                {!! Form::close() !!}
                                            @else
                                                {!! Form::open(['action' => ['ProfileController@toggleActivity'], 'method' => 'POST', 'class' => 'float-right pl-1']) !!}
                                                    {{ Form::hidden('action', 'activate')}}
                                                    {{ Form::hidden('user_id', $user->id)}}
                                                    {{ Form::submit('  Aktiviraj  ', ['class' => 'btn btn-sm btn-info'])}}
                                                {!! Form::close() !!}
                                            @endif
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            {{$users->links()}}
                            @else
                            Ni registriranih uporabnikov.
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
