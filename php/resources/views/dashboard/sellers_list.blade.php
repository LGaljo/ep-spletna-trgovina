@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-lg-9 col-sm-12">
                <h1 class="">{{$title}}</h1>

                <div class="card">
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if($sellers != null && count($sellers) > 0)
                            <ul class="list-group">
                                @foreach($sellers as $seller)
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        <p class="p-0 m-0 flex-grow-1">{{$seller->name}}</p>
                                        <div>
                                            <span class="badge badge-warning badge-pill mr-4">Prodajalec</span>
                                            <a role="button" class="btn btn-sm btn-success pull-right" href="/dashboard/{{$seller->id}}">Uredi
                                                podatke</a>
                                            @if($seller->active == true)
                                                {!! Form::open(['action' => ['ProfileController@toggleActivity'], 'method' => 'POST', 'class' => 'float-right pl-1']) !!}
                                                {{ Form::hidden('action', 'deactivate')}}
                                                {{ Form::hidden('user_id', $seller->id)}}
                                                {{ Form::submit('Deaktiviraj', ['class' => 'btn btn-sm btn-danger'])}}
                                                {!! Form::close() !!}
                                            @else
                                                {!! Form::open(['action' => ['ProfileController@toggleActivity'], 'method' => 'POST', 'class' => 'float-right pl-1']) !!}
                                                {{ Form::hidden('action', 'activate')}}
                                                {{ Form::hidden('user_id', $seller->id)}}
                                                {{ Form::submit('  Aktiviraj  ', ['class' => 'btn btn-sm btn-info'])}}
                                                {!! Form::close() !!}
                                            @endif
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            {{$sellers->links()}}
                        @else
                            Ni registriranih strank.
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
