@extends('layouts/app')
@section('content')
    <!-- https://laravelcollective.com/docs/6.0/html -->
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="">Uredi svoje podatke</h1>
                <div class="card">
                    <div class="card-body">
                        {!! Form::open(['action' => ['ProfileController@update'], 'method' => 'POST']) !!}
                            <div class="form-group">
                                {{Form::label('name', 'Ime in priimek')}}
                                {{Form::text('name', Auth::user()->name, ['class' => 'form-control', 'placeholder' => 'Ime Priimek'])}}
                            </div>

                            <div class="form-group">
                                {{Form::label('email', 'E-Mail')}}
                                {{Form::text('email', Auth::user()->email, ['class' => 'form-control', 'placeholder' => 'E-Mail'])}}
                            </div>

                            <div class="form-group">
                                {{Form::label('phone', 'Telefonska številka')}}
                                {{Form::text('phone', Auth::user()->phone, ['class' => 'form-control', 'placeholder' => 'Telefonska številka'])}}
                            </div>

                            {{Form::hidden('id', Auth::user()->id)}}
                            {{Form::hidden('_method', 'POST')}}
                            {{Form::submit('Posodobi', ['class' => 'btn btn-primary'])}}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
