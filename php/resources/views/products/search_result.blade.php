@extends('layouts/app')

@section('content')
    <div class="row">
        <div class="col-auto mr-auto">
            <h1 class="mt-5">Zadetki za {{$query}}</h1>
        </div>
        @if(!Auth::guest())
            <div class="col-auto">
                <a href="/products/create" class="mt-5 btn btn-dark">Dodaj produkt</a>
            </div>
        @endif
    </div>

    <div class="row">
        @if (count($products) >= 1)
            @foreach ($products as $product)
                <div class="col-md-4 pl-0">
                    <div class="card card-body bg-light mb-2">
                        <h3><a href="/products/{{$product->id}}">{{$product->name}}</a></h3>
                        <small>{{$product->description}}</small>
                        <h4 class="text-right">{{$product->price}} €</h4>

                        {!! Form::open(['action' => ['CartController@addToCart'], 'method' => 'POST', 'class' => 'form-inline']) !!}
                        {{ Form::number('quantity', 1, ['class' => 'form-control mr-2 col-4']) }}
                        {{ Form::hidden('product_id', $product->id) }}
                        {{ Form::submit('V košarico', ['class' => 'btn btn-primary col-6']) }}
                        {!! Form::close() !!}

                    </div>
                </div>
            @endforeach
        @else
            <p>Ni zadetkov</p>
        @endif
    </div>

@endsection
