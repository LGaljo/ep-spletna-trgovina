@extends('layouts/app')

@section('content')
    <div class="row">
        <div class="col-auto mr-auto">
            <h1 class="mt-5">Seznam produktov</h1>
        </div>
        @if(!Auth::guest() && Auth::user()->role->id > 1)
            <div class="col-auto">
                <a href="/products/create" class="mt-5 btn btn-dark">Dodaj produkt</a>
            </div>
        @endif
    </div>

    @if (count($products) >= 1)
        <div class="row">
            @foreach ($products as $product)
                <div class="col-md-4 col-xl-3 d-flex align-items-stretch">
                    <div class="card card-body bg-light mb-2 d-flax">
                        <h3><a href="/products/{{$product->id}}">{{$product->name}}</a></h3>
                        <small>{{$product->showDesc()}}</small>
                        <span>
                            @for($i=0; $i<$product->rating();$i++)
                                <i class="fa fa-star" style="color:#ffd54f"></i>
                            @endfor
                            @for($i=$product->rating();$i<5;$i++)
                                <i class="fa fa-star-o" style="color:#ffd54f"></i>
                            @endfor
                        </span>
                        <div class="flex-column mt-auto">
                            <h4 class="text-right">{{$product->price}} €</h4>
                            {!! Form::open(['action' => ['CartController@addToCart'], 'method' => 'POST', 'class' => 'form-inline']) !!}
                            {{ Form::number('quantity', 1, ['class' => 'form-control mr-2 col-3', 'min' => 1]) }}
                            {{ Form::hidden('product_id', $product->id) }}
                            {{ Form::submit('V košarico', ['class' => 'btn btn-primary col-5 col-md-7']) }}
                            {!! Form::close() !!}
                        </div>

                    </div>
                </div>
            @endforeach
        </div>
        <div class="row">
            <div style="margin-left: 25%; width:50%">
                {{$products->links()}}
            </div>
        </div>
    @else
        <div class="row">
            <p>Trgovina je trenutno prazna</p>
        </div>
    @endif

@endsection
