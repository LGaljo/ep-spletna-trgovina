<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'cart_id', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function shopping_cart() {
        return $this->hasOne('App\ShoppingCart');
    }

    public function ratings() {
        return $this->belongsTo('App\Ratings');
    }

    public function orders() {
        return $this->hasMany('App\Order');
    }

    public function role() {
        return $this->belongsTo('App\UserRole', 'role_id');
    }

    public function isAdmin() {
        return $this->role->id == 3;
    }

    public function isSeller() {
        return $this->role->id == 2;
    }

    public function isCustomer() {
        return $this->role->id == 1;
    }
}
