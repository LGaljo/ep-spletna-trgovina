<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    // Table name
    protected $table = 'order_statuses';

    protected $fillable = [
        'name'
    ];
}
