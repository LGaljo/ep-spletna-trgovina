<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingCart extends Model
{
    // table name
    protected $table = 'shopping_carts';

    protected $fillable = [
        'user_id'
    ];

    // OneToOne relation
    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    // OneToMany relation
    public function cart_items() {
        return $this->hasMany('App\ShoppingCartItem', 'cart_id');
    }
}
