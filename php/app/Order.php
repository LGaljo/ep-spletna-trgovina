<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    // Table name
    protected $table = 'orders';

    protected $fillable = [
        'order_status_id', 'user_id'
    ];

    // ManyToOne relation
    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    // OneToOne relation
    public function status() {
        return $this->belongsTo('App\OrderStatus', 'order_status_id');
    }

    // OneToMany relation
    public function orderItems() {
        return $this->hasMany('App\OrderItem');
    }

    public function sumItemsPrices() {
        $sum = 0;
        foreach ($this->orderItems as $item) {
            $sum += $item->getPriceSum();
        }

        return $sum;
    }
}
