<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    // Table name
    protected $table = 'ratings';

    protected $fillable = [
        'value', 'product_id', 'user_id'
    ];

    // ManyToOne relation
    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    // ManyToOne relation
    public function product() {
        return $this->belongsTo('App\Product', 'product_id');
    }
}
