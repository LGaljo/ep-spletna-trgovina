<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    // table name
    protected $table = 'user_roles';

    protected $fillable = [
        'name'
    ];

    // OneToMany relation
    public function users() {
        return $this->belongsToMany('App\Users');
    }
}
