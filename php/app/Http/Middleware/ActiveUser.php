<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ActiveUser
{
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if ($user && $user->active == false) {
            Auth::logout();
            return redirect('/')->with('error', 'Uporabnik ni aktiven.');
        }

        return $next($request);
    }
}
