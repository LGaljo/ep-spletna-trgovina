<?php

namespace App\Http\Controllers;

use App\ShoppingCartItem;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CartController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $cart_id = auth()->user()->cart_id;
        $items = ShoppingCartItem::all()->where('cart_id', $cart_id);
        return view('cart.index')->with(['items'=>$items]);
    }

    public function show($id)
    {
        if (auth()->user()->role_id != 3) {
            return redirect('/')->with('error', 'Nimate pravic za ogled košarice');
        }
        $items = ShoppingCartItem::all()->where('cart_id', $id);
        return view('cart.index')->with('items', $items);
    }

    public function destroy($id)
    {
        $product = ShoppingCartItem::find($id);
        $product->delete();

        return redirect('/cart')->with('success', 'Izdelek odstranjen iz košarice');
    }

    public function update(Request $request, $id)
    {
        // Update cart
        $cart_item = ShoppingCartItem::find($id);

        if ($request->input('action') == 'add') {
            $cart_item->quantity += 1;
        } else if ($request->input('action') == 'remove') {
            if ($cart_item->quantity > 1) {
                $cart_item->quantity -= 1;
            }
        }
        $cart_item->save();
        return redirect('/cart')->with('success', 'Posodobljeno');
    }

    public function addToCart(Request $request) {
        $this->validate($request, [
            'quantity' => 'required|numeric',
            'product_id' => 'required',
        ]);

        $quantity = $request->input('quantity');
        $pid = $request->input('product_id');

        if ($quantity <= 0) {
            return redirect('/')->with('error', 'Ni mogoče dodati negativnih količin.');
        }

        $cart = auth()->user()->shopping_cart;
        $items = $cart->cart_items;
        $item = $items->where('product_id', $pid)->first();
        if ($item) {
            $item->quantity += 1;
            $item->save();
        } else {
            $item = new ShoppingCartItem();
            $item->cart_id = auth()->user()->cart_id;
            $item->product_id = $pid;
            $item->quantity = $quantity;
            $item->save();
        }

        return redirect('/')->with('success', 'Dodano v košarico');
    }
}
