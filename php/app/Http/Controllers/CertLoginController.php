<?php

namespace App\Http\Controllers;

class CertLoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.x509');
    }

    /**
     * Redirects to app.
     */
    public function index()
    {
        return redirect('/');
    }
}
