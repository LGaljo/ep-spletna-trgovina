<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Order;
use App\OrderItem;
use App\ShoppingCartItem;
use Illuminate\Support\Facades\Auth;

class ApiOrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function finishOrder()
    {
        $user = Auth::guard('api')->user();
        $items = $user->shopping_cart->cart_items;

        $order = Order::create([
            'order_status_id' => 1,
            'user_id' => $user->id
        ]);

        foreach ($items as $item) {
            OrderItem::create([
                'quantity' => $item->quantity,
                'price' => $item->product->price,
                'order_id' => $order->id,
                'name' => $item->product->name,
                'product_id' => $item->product->id
            ]);
        }

        // Izprazni košarico
        ShoppingCartItem::where('cart_id', auth()->user()->cart_id)->delete();

        return response('OK', 200);
    }
}
