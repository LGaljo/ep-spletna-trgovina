<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderItem;
use App\ShoppingCartItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
    // Order statuses:
    //   0 nepotrjeno
    //   1 potrjeno
    //   2 stornirano

    // Za dashboard
    public function getAllOrders(){
        if (auth()->user()->role->id < 2) {
            // redirect back
            return redirect('/')->with('error', 'Nimate pravic za ogled strani');
        }

        $orders = Order::all();
        return view('dashboard.orders.orders')->with([
            'title' => 'Vsa naročila',
            'orders' => $orders
        ]);
    }

    public function changeStatus(Request $request, $id) {
        if (auth()->user()->role->id < 2) {
            // redirect back
            return redirect('/')->with('error', 'Nimate pravic za ogled strani');
        }

        $this->validate($request, [
            'action' => 'required'
        ]);

        $order = Order::find($id);
        $user = auth()->user();
        $role = $user->role;
        switch ($request->input(['action'])) {
            case 'confirm':
                if ($role->id == 3) {
                    Log::info("User {$user->name} [admin] confirmed an order.");
                } else if ($role->id == 2) {
                    Log::info("User {$user->name} [seller] confirmed an order.");
                }
                $order->order_status_id = 2;
                break;
            case 'cancel':
                if ($role->id == 3) {
                    Log::info("User {$user->name} [admin] canceled an order.");
                } else if ($role->id == 2) {
                    Log::info("User {$user->name} [seller] canceled an order.");
                }
                $order->order_status_id = 3;
                break;
            case 'remove':
                if ($role->id == 3) {
                    Log::info("User {$user->name} [admin] removed an order.");
                } else if ($role->id == 2) {
                    Log::info("User {$user->name} [seller] removed an order.");
                }
                Order::destroy($id);
                break;
        }

        $order->save();

        return redirect("/dashboard/orders")->with('success', 'Uspešno spremenjeno');
    }

    // Za uporabnika
    public function finishOrder(){
        $user = auth()->user();
        $items = $user->shopping_cart->cart_items;

        $order = Order::create([
            'order_status_id' => 1,
            'user_id' => $user->id
        ]);

        foreach ($items as $item) {
            OrderItem::create([
                'quantity' => $item->quantity,
                'price' => $item->product->price,
                'order_id' => $order->id,
                'name' => $item->product->name,
                'product_id' => $item->product->id
            ]);
        }

        // Izprazni košarico
        ShoppingCartItem::where('cart_id', auth()->user()->cart_id)->delete();

        return redirect('/')->with('success', 'Naročilo je bilo oddano, počakajte na potrditev.');
    }

    public function getOrder($id){
        $order = Order::find($id);

        return view('dashboard.orders.order')->with([
            'title' => "Naročilo $id",
            'order' => $order,
        ]);
    }

    public function getAllOrdersByUser(){
        return view('dashboard.orders.past_orders')->with('title', "Pretekla naročila");
    }
}
