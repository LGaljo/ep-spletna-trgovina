<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use FullTextSearch;

    // Table name
    protected $table = 'products';

    protected $fillable = [
        'price', 'name', 'description', 'active'
    ];

    protected $searchable = [
        'name',
    ];

    public function ratings() {
        return $this->hasMany('App\Rating');
    }

    public function images() {
        return $this->hasMany('App\Image');
    }

    public function showDesc() {
        if (strlen($this->description) < 200) {
            return $this->description;
        }
        return substr($this->description, 0, 230) . '...';
    }

    public function rating() {
        $all = $this->ratings;
        $sum = 0;

        foreach($all as $r) {
            $sum += $r->value;
        }

        if (count($all) == 0) {
            return 0;
        }

        return round($sum / count($all));
    }
}
