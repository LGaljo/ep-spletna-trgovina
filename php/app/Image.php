<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    // Table name
    protected $table = 'images';

    protected $fillable = [
        'image', 'product_id'
    ];

    // ManyToOne relation
    public function product() {
        return $this->belongsTo('App\Product');
    }
}
