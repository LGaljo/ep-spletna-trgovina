package si.ep.spletnatrgovina.models;

public class ProductResponse {
    private Product data;

    public ProductResponse() {
    }

    public Product getData() {
        return data;
    }

    public void setData(Product data) {
        this.data = data;
    }
}
