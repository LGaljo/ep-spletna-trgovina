package si.ep.spletnatrgovina.ui.fragments;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import si.ep.spletnatrgovina.R;
import si.ep.spletnatrgovina.adapters.ProductListAdapter;
import si.ep.spletnatrgovina.models.Product;
import si.ep.spletnatrgovina.viewmodels.ProductsViewModel;

import static si.ep.spletnatrgovina.common.Common.BEARER;

public class MainFragment extends Fragment {
    private static final String TAG = MainFragment.class.getSimpleName();

    @BindView(R.id.product_list_view)
    RecyclerView recyclerView;

    private ProductsViewModel mViewModel;
    private ProductListAdapter mAdapter;
    private ArrayList<Product> productArrayList = new ArrayList<>();

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, container, false);
        ButterKnife.bind(this, view);

        mAdapter = new ProductListAdapter(productArrayList, getContext(), getActivity());

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(getContext()), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);

        setHasOptionsMenu(true);

        return view;
    }

    private void fetchData() {
        String token = Objects.requireNonNull(
                PreferenceManager.getDefaultSharedPreferences(getContext())
                        .getString(BEARER, ""));

        mViewModel.getProducts(token).observe(this, data -> {
            if (data == null || data.getData() == null) return;
            if (data.getData().isEmpty()) {
                Toast.makeText(getContext(), getString(R.string.no_data_to_show), Toast.LENGTH_LONG).show();
                Log.d(TAG, "No data available to show!");
            } else {
                // update UI
                productArrayList.clear();
                productArrayList.addAll(data.getData());
                mAdapter.notifyDataSetChanged();
                Log.d(TAG, "List updated");
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ProductsViewModel.class);

        fetchData();
    }

    public void onCreateOptionsMenu(@NotNull Menu menu, @NotNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search_menu, menu);

        SearchManager searchManager = (SearchManager) Objects.requireNonNull(getActivity()).getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        assert searchManager != null;
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                // Log.d(TAG, "onQueryTextSubmit");
                mAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                // Log.d(TAG, "onQueryTextChange");
                mAdapter.getFilter().filter(query);
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
