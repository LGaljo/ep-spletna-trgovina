package si.ep.spletnatrgovina.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import si.ep.spletnatrgovina.BuildConfig;
import si.ep.spletnatrgovina.R;
import si.ep.spletnatrgovina.interfaces.FragmentChangeListener;
import si.ep.spletnatrgovina.models.Product;
import si.ep.spletnatrgovina.ui.fragments.DetailFragment;

import static si.ep.spletnatrgovina.common.Common.INTENT_PRODUCT_ID;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> implements Filterable {
    private static final String TAG = ProductListAdapter.class.getSimpleName();

    private FragmentChangeListener fragmentChangeListener;
    private ArrayList<Product> items_all;
    private ArrayList<Product> items_to_show;
    private Context context;

    /**
     * Filtriranje rezultatov iskanja na listu vseh interesnih točk
     *
     * @return results
     */
    @Override
    public Filter getFilter() {
        Log.d(TAG, "getFilter Filter now");
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();

                if (charString.isEmpty()) {
                    items_to_show = items_all;
                } else {
                    ArrayList<Product> filteredList = new ArrayList<>();
                    for (Product l : items_all) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (l.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(l);
                        }
                    }

                    items_to_show = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = items_to_show;
                return filterResults;
            }

            @Override
            @SuppressWarnings("unchecked")
            protected void publishResults(CharSequence constraint, FilterResults results) {
                items_to_show = (ArrayList<Product>) results.values;
                Log.d(TAG, "publishResults: Show " + items_to_show);

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        ImageView item_pic;
        TextView item_name;
        TextView item_summary;
        Integer productId;
        View view;

        ViewHolder(View view) {
            super(view);
            this.item_pic = view.findViewById(R.id.item_pic);
            this.item_name = view.findViewById(R.id.item_name);
            this.item_summary = view.findViewById(R.id.item_summary);
            this.view = view;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ProductListAdapter(ArrayList<Product> items_all, Context context, Activity activity) {
        this.context = context;
        this.items_all = items_all;
        this.items_to_show = items_all;

        if (activity instanceof FragmentChangeListener) {
            fragmentChangeListener = (FragmentChangeListener) activity;
        } else {
            // Find your bugs early by making them clear when you can...
            if (BuildConfig.DEBUG) {
                throw new IllegalArgumentException("Fragment hosts must implement FragmentChangeListener");
            }
        }
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public ProductListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_list_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Product product = items_to_show.get(position);
        View view = holder.view;
        holder.item_name.setText(product.getName());
        holder.item_summary.setText(Html.fromHtml(product.getDescription(), Html.FROM_HTML_MODE_LEGACY));
        holder.productId = product.getId();

        view.setOnClickListener((View v) -> notifyFragmentChange(product));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items_to_show.size();
    }

    private void notifyFragmentChange(Product product) {
        Bundle bundle = new Bundle();
        bundle.putInt(INTENT_PRODUCT_ID, product.getId());
        Fragment fragment = new DetailFragment();
        fragment.setArguments(bundle);

        FragmentChangeListener listener = fragmentChangeListener;
        if (listener != null) {
            listener.onFragmentChangeRequested(fragment);
        }
    }
}
