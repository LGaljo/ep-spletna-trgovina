package si.ep.spletnatrgovina.common;

public class Common {
    public static String USERINFO_NAME = "si.ep.spletnatrgovina.static.USERNAME";
    public static String USERINFO_EMAIL = "si.ep.spletnatrgovina.static.USER_EMAIL";
    public static String CART_ITEM_ID = "si.ep.spletnatrgovina.static.CART_ITEM_ID";
    public static String QUANTITY = "si.ep.spletnatrgovina.static.QUANTITY";

    public static String BEARER = "si.ep.spletnatrgovina.static.BEARER";

    public static String INTENT_PRODUCT_ID = "si.ep.spletnatrgovina.static.product_id";
    public static final String BASE_URL = "http://10.0.2.2/api/";
}
