package si.ep.spletnatrgovina.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.NumberPicker;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import si.ep.spletnatrgovina.R;
import si.ep.spletnatrgovina.interfaces.DialogFragmentDismissEvent;
import si.ep.spletnatrgovina.retrofit.CartAPI;
import si.ep.spletnatrgovina.retrofit.RetrofitFactory;

import static si.ep.spletnatrgovina.common.Common.BASE_URL;
import static si.ep.spletnatrgovina.common.Common.BEARER;
import static si.ep.spletnatrgovina.common.Common.CART_ITEM_ID;
import static si.ep.spletnatrgovina.common.Common.QUANTITY;

public class CartDialogFragment extends DialogFragment implements DialogInterface.OnDismissListener {
    private static final String TAG = CartDialogFragment.class.getSimpleName();

    DialogFragmentDismissEvent dismissEvent;

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle bundle = getArguments();
        int pid = Objects.requireNonNull(bundle).getInt(CART_ITEM_ID);
        int quantity = Objects.requireNonNull(bundle).getInt(QUANTITY);

        NumberPicker numberPicker = new NumberPicker(getActivity());
        numberPicker.setWrapSelectorWheel(true);
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(20);
        numberPicker.setValue(quantity);

        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()))
                .setTitle(getTag())
                .setMessage(getString(R.string.choose_item_number))
                .setPositiveButton(R.string.save, (dialog, which) -> saveChange(numberPicker.getValue(), pid))
                .setNegativeButton(R.string.cancel, (dialog, which) -> dialog.cancel())
                .setNeutralButton(R.string.remove, (dialog, id) -> removeItemFromCart(pid))
                .setView(numberPicker);

        return builder.create();
    }

    public void setListener(DialogFragmentDismissEvent listener){
        dismissEvent = listener;
    }

    @Override
    public void onDismiss(@NotNull final DialogInterface dialog) {
        Log.d(TAG, "onDismiss dialog fragment");
        super.onDismiss(dialog);
        final Activity activity = getActivity();
        if (activity instanceof DialogFragmentDismissEvent) {
            ((DialogInterface.OnDismissListener) activity).onDismiss(dialog);
        }
        dismissEvent.dismissEvent();
    }

    private void removeItemFromCart(int pid) {
        String token = Objects.requireNonNull(
                PreferenceManager.getDefaultSharedPreferences(getContext())
                        .getString(BEARER, ""));

        Retrofit retrofit = RetrofitFactory.getInstance(BASE_URL);
        CartAPI cartAPI = retrofit.create(CartAPI.class);

        Call<String> call;
        call = cartAPI.removeItemFromCart(pid, token);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NotNull Call<String> call, @NotNull Response<String> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Log.d(TAG, "Košarica posodobljena");
                } else {
                    Log.d(TAG, "Request failed\n" + response.errorBody());
                }
            }

            @Override
            public void onFailure(@NotNull Call<String> call, @NotNull Throwable t) {
                Log.e(TAG, t.getMessage(), t);
            }
        });
    }

    private void saveChange(int quantity, int pid) {
        String token = Objects.requireNonNull(
                PreferenceManager.getDefaultSharedPreferences(getContext())
                        .getString(BEARER, ""));

        Retrofit retrofit = RetrofitFactory.getInstance(BASE_URL);
        CartAPI cartAPI = retrofit.create(CartAPI.class);

        Call<String> call;
        call = cartAPI.updateCartItem(pid, quantity, token);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NotNull Call<String> call, @NotNull Response<String> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Log.d(TAG, "Košarica posodobljena");
                } else {
                    Log.d(TAG, "Request failed" + response.errorBody());
                }
            }

            @Override
            public void onFailure(@NotNull Call<String> call, @NotNull Throwable t) {
                Log.e(TAG, t.getMessage(), t);
            }
        });

    }
}
