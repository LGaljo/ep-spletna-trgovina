package si.ep.spletnatrgovina.viewmodels;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import si.ep.spletnatrgovina.models.Cart;
import si.ep.spletnatrgovina.retrofit.CartAPI;
import si.ep.spletnatrgovina.retrofit.RetrofitFactory;

import static si.ep.spletnatrgovina.common.Common.BASE_URL;

public class CartViewModel extends ViewModel implements Callback<Cart> {
    private static final String TAG = CartViewModel.class.getSimpleName();

    private MutableLiveData<Cart> cart;

    private CartAPI cartAPI;
    private Retrofit retrofit;

    public LiveData<Cart> getCart(String token) {
        if (retrofit == null) {
            retrofit = RetrofitFactory.getInstance(BASE_URL);
            cartAPI = retrofit.create(CartAPI.class);
        }

        if (cart == null) {
            cart = new MutableLiveData<>();
            load(token);
        }
        return cart;
    }

    private void load(String token) {
        Call<Cart> call;
        call = cartAPI.getCart(token);
        call.enqueue(this);
    }

    @Override
    public void onResponse(@NotNull Call<Cart> call, @NotNull Response<Cart> response) {
        if (response.isSuccessful() && response.body() != null) {
            cart.setValue(response.body());
        }
    }

    @Override
    public void onFailure(@NotNull Call<Cart> call, @NotNull Throwable t) {
        Log.d(TAG, "onFailure: " + t.getLocalizedMessage());
    }
}
