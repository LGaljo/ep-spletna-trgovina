package si.ep.spletnatrgovina.ui.main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.navigation.NavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import si.ep.spletnatrgovina.R;
import si.ep.spletnatrgovina.interfaces.ChangeTitle;
import si.ep.spletnatrgovina.interfaces.FragmentChangeListener;
import si.ep.spletnatrgovina.ui.fragments.CartFragment;
import si.ep.spletnatrgovina.ui.fragments.MainFragment;
import si.ep.spletnatrgovina.ui.login.LoginActivity;

import static si.ep.spletnatrgovina.common.Common.BEARER;
import static si.ep.spletnatrgovina.common.Common.USERINFO_EMAIL;
import static si.ep.spletnatrgovina.common.Common.USERINFO_NAME;

public class MainActivity extends AppCompatActivity implements FragmentChangeListener, ChangeTitle {
    private SharedPreferences sharedPreferences;
    private static final String TAG = MainActivity.class.getSimpleName();
    private Class fragmentClass = MainFragment.class;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        setSupportActionBar(toolbar);
        setTitle(getString(R.string.title_name));
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if (savedInstanceState == null) {
            try {
                Class currentFragment = MainFragment.class;
                Fragment fragment = (Fragment) currentFragment.newInstance();
                getSupportFragmentManager().beginTransaction().add(R.id.main_framelayout, fragment).commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        setupDrawerContent();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        // Najprej zapri drawer
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            // Nato pojdi v MapFragment
            // TODO ask for exit
//            if (fragmentClass != MapFragment.class) {
//                try {
//                    fragmentClass = MapFragment.class;
//                    Fragment fragment = (Fragment) fragmentClass.newInstance();
//                    FragmentManager fragmentManager = getSupportFragmentManager();
//                    fragmentManager.beginTransaction().replace(R.id.main_framelayout, fragment).commit();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            } else {
                // Na koncu izhod
                super.onBackPressed();
//            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    private void setupDrawerContent() {
        View headerView = navigationView.getHeaderView(0);

        // View
        TextView username = headerView.findViewById(R.id.text_view_name);
        TextView email = headerView.findViewById(R.id.text_view_email);

        headerView.setOnClickListener((View v) -> {
            Intent intent = new Intent(this, EditProfile.class);
            startActivity(intent);
        });

        // Set username & email
        if (username != null && email != null) {
            username.setText(sharedPreferences.getString(USERINFO_NAME, ""));
            email.setText(sharedPreferences.getString(USERINFO_EMAIL, ""));
        }

        navigationView.setNavigationItemSelectedListener((MenuItem menuItem) -> {
            fragmentClass = MainFragment.class;
            switch (menuItem.getItemId()) {
                case R.id.nav_list:
                    fragmentClass = MainFragment.class;
                    break;
                case R.id.nav_cart:
                    fragmentClass = CartFragment.class;
                    break;
                case R.id.nav_logout:
                    Toast.makeText(this, getString(R.string.logged_out_message), Toast.LENGTH_SHORT).show();
                    logout();
                    break;
            }

            try {
                Fragment fragment = (Fragment) fragmentClass.newInstance();
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.main_framelayout, fragment).commit();
            } catch (Exception e) {
                e.printStackTrace();
            }

            drawer.closeDrawer(GravityCompat.START);
            return true;
        });
    }

    private void logout() {
        // remove token and go to login activity
        sharedPreferences.edit().remove(BEARER).apply();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

    }

    // My attempt at making it possible to change displayed fragment from within fragments
    public void changeDetailFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.addToBackStack(null);
        transaction.replace(R.id.main_framelayout, fragment);
        transaction.commit();
    }

    @Override
    // This is the interface implementation that will be called by your fragments
    public void onFragmentChangeRequested(Fragment newFragment) {
        changeDetailFragment(newFragment);
    }

    @Override
    public void replaceTitle(String title) {
        setTitle(title);
    }
}
