package si.ep.spletnatrgovina.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import si.ep.spletnatrgovina.models.ProductResponse;
import si.ep.spletnatrgovina.models.ProductsResponse;

public interface ProductsAPI {
    @Headers("Accept: application/json")
    @GET("products")
    Call<ProductsResponse> getProducts(@Header("Authorization") String bearer);

    @Headers("Accept: application/json")
    @GET("products/{product_id}")
    Call<ProductResponse> getProduct(
            @Path("product_id") int product_id,
            @Header("Authorization") String bearer
    );
}
