package si.ep.spletnatrgovina.ui.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import si.ep.spletnatrgovina.R;
import si.ep.spletnatrgovina.models.AuthResponse;
import si.ep.spletnatrgovina.models.User;
import si.ep.spletnatrgovina.retrofit.AuthAPI;
import si.ep.spletnatrgovina.retrofit.ProfileAPI;
import si.ep.spletnatrgovina.retrofit.RetrofitFactory;
import si.ep.spletnatrgovina.ui.main.MainActivity;

import static si.ep.spletnatrgovina.common.Common.BASE_URL;
import static si.ep.spletnatrgovina.common.Common.BEARER;
import static si.ep.spletnatrgovina.common.Common.USERINFO_EMAIL;
import static si.ep.spletnatrgovina.common.Common.USERINFO_NAME;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = LoginActivity.class.getSimpleName();
    private SharedPreferences sharedPreferences;

    @BindView(R.id.loading)
    ProgressBar progressBar;

    @BindView(R.id.username)
    EditText emailField;

    @BindView(R.id.password)
    EditText passwordField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        // if shared prefs contains token, skip
        String token = sharedPreferences.getString(BEARER, null);
        if (token != null) {
            Toast.makeText(this, "Bili ste samodejno vpisani", Toast.LENGTH_SHORT).show();
            continueToApp();
        } else {
            Log.d(TAG, "onCreate: Token is null");
        }
    }

    @OnClick(R.id.login)
    protected void login() {
        Retrofit retrofit = RetrofitFactory.getInstance(BASE_URL);
        AuthAPI authAPI = retrofit.create(AuthAPI.class);

        progressBar.setVisibility(View.VISIBLE);

        // TODO require fields
        String email = emailField.getText().toString();
        String password = passwordField.getText().toString();

        Call<AuthResponse> call;
        call = authAPI.getBearerToken(email, password);
        call.enqueue(new Callback<AuthResponse>() {
            @Override
            public void onResponse(@NotNull Call<AuthResponse> call, @NotNull Response<AuthResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Toast.makeText(getApplicationContext(), "Uspešna prijava", Toast.LENGTH_LONG).show();
                    String token = response.body().getBearer();
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit()
                            .putString(BEARER, "Bearer " + token).apply();
                    getUserInfo(token);
                    continueToApp();
                } else if (response.code() == 401) {
                    Toast.makeText(getApplicationContext(), "Napačno geslo", Toast.LENGTH_LONG).show();
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NotNull Call<AuthResponse> call, @NotNull Throwable t) {
                Log.d(TAG, "Received failure");
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void getUserInfo(String token) {
        Retrofit retrofit = RetrofitFactory.getInstance(BASE_URL);
        ProfileAPI profileAPI= retrofit.create(ProfileAPI.class);

        Call<User> call;
        call = profileAPI.getUserProfile("Bearer " + token);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NotNull Call<User> call, @NotNull Response<User> response) {
                Log.d(TAG, "onResponse: HERE");
                if (response.isSuccessful() && response.body() != null) {
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit()
                            .putString(USERINFO_NAME, response.body().getName())
                            .putString(USERINFO_EMAIL, response.body().getEmail()).apply();
                } else {
                    Log.d(TAG, "Request failed\n" + response.errorBody());
                }
            }

            @Override
            public void onFailure(@NotNull Call<User> call, @NotNull Throwable t) {
                Log.e(TAG, t.getMessage(), t);
            }
        });
    }

    private void continueToApp() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
