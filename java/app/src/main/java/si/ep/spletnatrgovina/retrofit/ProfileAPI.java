package si.ep.spletnatrgovina.retrofit;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import si.ep.spletnatrgovina.models.User;

public interface ProfileAPI {
    @Headers("Accept: application/json")
    @GET("user")
    Call<User> getUserProfile(@Header("Authorization") String bearer);

    @Headers("Accept: application/json")
    @POST("user")
    Call<String> updateUserProfile(
            @Body User user,
            @Header("Authorization") String bearer
    );
}
