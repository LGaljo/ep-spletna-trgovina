package si.ep.spletnatrgovina.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Locale;

import si.ep.spletnatrgovina.R;
import si.ep.spletnatrgovina.interfaces.ShowDialog;
import si.ep.spletnatrgovina.models.Cart;
import si.ep.spletnatrgovina.models.CartItem;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {
    private static final String TAG = CartAdapter.class.getSimpleName();

    private Cart cart;
    private Context context;
    private ShowDialog showDialog;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        ImageView item_pic;
        TextView item_name;
        TextView item_price;
        TextView item_sum_price;
        TextView item_quantity;
        View view;

        ViewHolder(View view) {
            super(view);
            this.item_pic = view.findViewById(R.id.item_pic);
            this.item_name = view.findViewById(R.id.item_name);
            this.item_price = view.findViewById(R.id.item_price);
            this.item_sum_price = view.findViewById(R.id.item_sum_price);
            this.item_quantity = view.findViewById(R.id.item_quantity);
            this.view = view;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CartAdapter(Cart cart, Context context, ShowDialog showDialog) {
        this.context = context;
        this.cart = cart;
        this.showDialog = showDialog;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public CartAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_list_item, parent, false);

        return new CartAdapter.ViewHolder(itemView);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull CartAdapter.ViewHolder holder, int position) {
        CartItem cartItem = cart.getCart_items().get(position);
        holder.item_name.setText(cartItem.getProduct().getName());
        holder.item_price.setText(String.format("%.2f €", cartItem.getProduct().getPrice()));
        holder.item_sum_price.setText(String.format("%.2f €", cartItem.getProduct().getPrice() * cartItem.getQuantity()));
        holder.item_quantity.setText(String.format("%d", cartItem.getQuantity()));

        View view = holder.view;
        view.setOnClickListener((View v) -> showDialog.showDialog(cartItem.getProduct().getName(), cartItem));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return cart.getCart_items().size();
    }

    public void setData(Cart cart) {
        this.cart = cart;
        notifyDataSetChanged();
    }

    public void clearList() {
        this.cart.getCart_items().clear();
        notifyDataSetChanged();
    }
}
