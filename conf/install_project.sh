#!/bin/bash

# Update apt
apt update

# Download nodejs setup files
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -

# Install required packages
apt install -y nodejs gcc g++ make composer php7.2-mbstring php7.2-gd php7.2-intl php7.2-xsl

# Configure apache2
cp ./001-ep.conf /etc/apache2/sites-available/001-ep.conf
cp ./cert/* /etc/apache2/ssl/

# Change directory to ./php
cd ../php

# Enable site
a2dissite 000-default
a2ensite 001-ep
a2enmod ssl
a2enmod rewrite

# Restart apache2 server
service apache2 restart

# Install and compile npm dependencies
npm install
npm run dev

# Install composer dependencies
composer install

# Add laravel database to mysql instance
mysql -u root -pep -e 'CREATE DATABASE IF NOT EXISTS laravel'

# Migrate data
php artisan migrate:refresh
php artisan db:seed
php artisan storage:link

# Generate Laravel key
php artisan key:generate

# Set some permissions
chown -R $USER:www-data .
chmod -R 755 .
