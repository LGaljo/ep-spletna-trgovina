-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Gostitelj: 127.0.0.1
-- Čas nastanka: 13. jan 2020 ob 05.20
-- Različica strežnika: 10.4.8-MariaDB
-- Različica PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Zbirka podatkov: `laravel`
--

-- --------------------------------------------------------

--
-- Struktura tabele `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabele `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabele `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Odloži podatke za tabelo `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(493, '2014_10_12_000000_create_users_table', 1),
(494, '2014_10_12_100000_create_password_resets_table', 1),
(495, '2019_08_19_000000_create_failed_jobs_table', 1),
(496, '2019_12_03_140203_create_shopping_carts_table', 1),
(497, '2019_12_03_141524_create_orders_table', 1),
(498, '2019_12_03_141532_create_order_items_table', 1),
(499, '2019_12_03_141551_create_ratings_table', 1),
(500, '2019_12_03_141604_create_shopping_cart_items_table', 1),
(501, '2019_12_03_141622_create_products_table', 1),
(502, '2019_12_03_141638_create_order_statuses_table', 1),
(503, '2019_12_03_143141_create_user_roles_table', 1),
(504, '2019_12_27_110716_add_users', 1),
(505, '2020_01_12_000018_create_images_table', 1);

-- --------------------------------------------------------

--
-- Struktura tabele `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order_status_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabele `order_items`
--

CREATE TABLE `order_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `price` double NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabele `order_statuses`
--

CREATE TABLE `order_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Odloži podatke za tabelo `order_statuses`
--

INSERT INTO `order_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Nepotrjeno', '2020-01-13 03:20:06', '2020-01-13 03:20:06'),
(2, 'Potrjeno', '2020-01-13 03:20:06', '2020-01-13 03:20:06'),
(3, 'Stornirano', '2020-01-13 03:20:06', '2020-01-13 03:20:06');

-- --------------------------------------------------------

--
-- Struktura tabele `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabele `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Odloži podatke za tabelo `products`
--

INSERT INTO `products` (`id`, `created_at`, `updated_at`, `name`, `price`, `description`, `active`) VALUES
(1, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'aperiam ratione', 9.56, 'Nesciunt consequuntur illo sint cupiditate. Qui in nihil eveniet. Animi eius aut temporibus repudiandae. Ut suscipit fugit dolore tenetur.', 1),
(2, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'voluptate atque', 38.62, 'Qui molestiae beatae mollitia velit et non aut. Dolorum ut et placeat quia ut. Commodi inventore necessitatibus quo sed. Vitae autem nam autem.', 1),
(3, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'voluptatem placeat', 40.82, 'Dolores repellat rerum aut. Eos sunt iusto minima.', 1),
(4, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'dolorem repudiandae', 45.8, 'Nulla atque minus perferendis excepturi quo illum porro at. Voluptatem velit ratione reprehenderit eligendi eligendi eos consequatur sint. Hic assumenda enim cum.', 1),
(5, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'inventore sit', 3.93, 'Dolore incidunt rerum dignissimos quidem quibusdam reprehenderit omnis. Est voluptatem eveniet qui nulla at quia voluptatem vel. Illo voluptates eligendi quia deserunt et molestiae non.', 1),
(6, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'consequatur quisquam', 39.78, 'Voluptas illo debitis non nulla a. Eos sit velit sit tempora quod rerum. Eaque velit soluta aut sed laudantium. Magnam aut ut accusantium magnam animi.', 1),
(7, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'adipisci repudiandae', 21.82, 'Enim consectetur enim blanditiis qui praesentium. Perferendis accusamus dicta eaque et. Maiores est maiores aut rerum cum. Rem ab reiciendis molestiae laborum cumque.', 1),
(8, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'dicta est', 37.95, 'Ipsum dolore repellendus aut debitis sapiente earum sit. Ad quasi molestiae aut. Accusamus recusandae est quam qui ullam est ea.', 1),
(9, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'laborum qui', 14.31, 'Eligendi eos illo ea quasi saepe similique magnam. Qui accusantium blanditiis illum quis voluptate aspernatur deleniti. Reprehenderit aut minima enim nam asperiores.', 1),
(10, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'amet consequatur', 28.15, 'Saepe delectus laboriosam alias atque. Est porro neque quis est omnis. Nihil et dolorem est reiciendis officiis qui ad dolor. Dignissimos ea ratione et perspiciatis numquam.', 1),
(11, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'deserunt nihil', 4.21, 'Autem placeat sint id pariatur non praesentium et. Unde iure magni ut omnis. Numquam architecto nihil consectetur et. Totam laborum minus id enim nihil minus.', 1),
(12, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'quis qui', 0.52, 'Aliquam nulla quo rerum fugiat totam. Vitae omnis in animi incidunt. Quibusdam accusamus aliquid illo quaerat fugiat ut. Sint eos nesciunt quis consequatur ratione ex.', 1),
(13, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'unde accusantium', 32.73, 'Saepe impedit et in ratione et blanditiis dignissimos. Molestiae voluptatum dolorem iusto aliquid quas in repudiandae. Repudiandae sunt dolor suscipit doloremque sunt.', 1),
(14, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'autem incidunt', 17.33, 'Omnis nisi ipsam exercitationem enim praesentium et accusamus. Non error reiciendis perspiciatis dolores repellendus recusandae voluptatibus. Qui voluptatem enim minus illum aut accusamus.', 1),
(15, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'necessitatibus veniam', 28.61, 'Sint adipisci voluptatem voluptatum minus in. Rerum delectus distinctio ad eos occaecati. Et fugit placeat aliquam nostrum quam debitis ut.', 1),
(16, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'qui aut', 40.59, 'Vitae ea nesciunt dignissimos cumque molestiae nisi. At suscipit molestiae consequuntur minus cum. Beatae omnis quos omnis aperiam deserunt commodi perspiciatis alias.', 1),
(17, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'consequuntur dolorem', 45.07, 'Rerum sint dolor vitae vel voluptates doloribus. Consequatur reiciendis facere atque voluptate placeat eos eos id. Architecto minus sed et incidunt voluptas.', 1),
(18, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'ea ut', 35.84, 'Repudiandae delectus qui tenetur corporis architecto. Quisquam tempora ut qui voluptatem. Adipisci commodi eum incidunt illum suscipit ipsa.', 1),
(19, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'inventore maiores', 4.99, 'Ipsum quia quos et velit. Veniam est aliquid dolor. Magnam mollitia eos eum illum voluptas exercitationem veritatis sed.', 1),
(20, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'aut alias', 48.37, 'Officiis voluptatem nisi et ut rem repudiandae vitae. Veritatis temporibus vel velit officiis sed perferendis. Nihil ducimus enim quis dolore voluptatem. Veniam ipsa repellat saepe cupiditate aspernatur.', 1),
(21, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'consectetur nihil', 26.09, 'Quia voluptates maxime ea itaque. Qui magnam dolor quo sunt sit sunt impedit voluptatem. Est quod quibusdam ex tenetur.', 1),
(22, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'omnis blanditiis', 28.96, 'Rerum porro maxime et est voluptatum dolor. A vel et nisi. Animi accusantium voluptatem quis quia sit in.', 1),
(23, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'quia temporibus', 16.86, 'Omnis consequatur velit voluptas. Itaque esse a dolorem odit et. Perferendis odio veritatis voluptatum voluptas. Velit itaque consectetur doloribus quia quibusdam perspiciatis.', 1),
(24, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'non minima', 28.54, 'Dolore sunt rerum aut et repellendus dolores fugit. Eligendi consequuntur enim sunt voluptates quia. Rerum qui quibusdam omnis voluptas ut repudiandae. Ipsa dolore sapiente non accusantium quasi voluptas voluptas non.', 1),
(25, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'qui cum', 16.1, 'Ut voluptatem quod iusto ut sed. Est fugiat pariatur at nam. Sed deleniti et autem culpa provident delectus quia. Earum aliquam iste eius tenetur ipsa.', 1),
(26, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'esse libero', 2.42, 'Rerum illum qui illum ipsa quod ut. Quia consequatur culpa dolorum vitae. Necessitatibus voluptatibus voluptas voluptas.', 1),
(27, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'rerum omnis', 46.24, 'Voluptas qui alias velit laudantium consectetur natus. Molestiae quasi sit quibusdam et. Aliquam officiis accusantium sapiente et. Ipsam suscipit omnis iste est laboriosam dignissimos quisquam.', 1),
(28, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'veritatis veritatis', 16.86, 'Aperiam at pariatur commodi aspernatur numquam quo et. Amet ducimus et sunt nam nam in dolorem aliquam. Et tempora laboriosam voluptas consequatur similique laboriosam laboriosam sed. Cupiditate quidem optio voluptatem tempore iure repudiandae dolores.', 1),
(29, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'aut molestiae', 26.24, 'Sit sint distinctio vero non commodi eveniet. Exercitationem deserunt ea ut molestiae sed voluptate placeat. Tenetur omnis mollitia fugiat natus voluptatem possimus vel. Mollitia et molestiae iusto hic optio praesentium non.', 1),
(30, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'et voluptatem', 18.41, 'Nulla hic non dolorem eius sed eos odio inventore. Officia architecto nobis voluptatibus iure et soluta laudantium quo. Est velit ipsa totam et. Qui praesentium dolore est possimus enim consequatur.', 1),
(31, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'perspiciatis laborum', 1.09, 'Id iste est aut aut quia quis. Laboriosam omnis eos aut earum quasi voluptatem. Dolor possimus non tenetur labore culpa et.', 1),
(32, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'debitis sit', 34.2, 'Vitae libero velit veniam quia quo consequuntur eaque. Vel quis eum quas qui quam tenetur. Dolores quo architecto delectus et.', 1),
(33, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'quo quis', 23.42, 'Culpa repudiandae quia rerum quia culpa. Blanditiis minus consequuntur occaecati veritatis sit velit. Sed quia sit ut rerum tempora.', 1),
(34, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'iusto et', 2.18, 'Consequatur dolores eligendi iste velit. Iusto voluptatem aspernatur unde aut et labore qui. Tempore repellendus ab quibusdam mollitia dicta.', 1),
(35, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'reiciendis optio', 35.16, 'Modi perspiciatis hic eligendi nihil sit. Amet modi quia explicabo facere provident. Quo nemo velit praesentium rem optio quas sequi. Iste repudiandae sint ab distinctio optio rerum iure.', 1),
(36, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'exercitationem esse', 37, 'Qui optio rerum iure repellat aut tenetur. Quas earum itaque minima. Similique laborum rerum voluptatem optio ut.', 1),
(37, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'enim tenetur', 42.55, 'Quia labore ut distinctio. Quae sed eaque repudiandae voluptatem. Maiores aut illo temporibus qui aut voluptatem doloremque.', 1),
(38, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'consectetur maiores', 5.61, 'Sequi ratione ullam sint eos. Aut illum iusto velit exercitationem neque incidunt sed. Ad eos veniam quia perferendis deleniti laboriosam eos. Corporis molestias quaerat odit debitis.', 1),
(39, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'ipsam quaerat', 21.29, 'Excepturi in molestiae enim quia. Neque earum quos quasi qui doloremque. Quaerat voluptas quibusdam assumenda repellat nihil voluptatem consequatur. Non consequatur totam suscipit.', 1),
(40, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'eos repudiandae', 1.56, 'Quam commodi libero nesciunt a. Est aut porro fuga. Deleniti fugit aut molestiae molestiae. Dolores explicabo odio ut quae ex ipsum.', 1),
(41, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'nemo in', 21.61, 'Possimus incidunt sint blanditiis temporibus quo est rerum ullam. Quo qui eveniet et tempora quisquam ut similique. Et in et nemo nostrum. Quisquam est mollitia eligendi quia quos.', 1),
(42, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'assumenda qui', 13.63, 'Numquam reiciendis deserunt ducimus voluptate. Id dolorum dolor minima aut aliquam. Quos iste quas molestias voluptates voluptatem. Itaque ut distinctio veritatis debitis ab.', 1),
(43, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'beatae voluptas', 4.53, 'Debitis eaque sequi ut quis voluptatem dolorem. Eos consequatur ad sed occaecati beatae cum. Quam reprehenderit iste sint voluptas quaerat. Quae non enim qui asperiores tenetur quaerat aperiam.', 1),
(44, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'error unde', 29.05, 'Tempora porro eius dolor vel quasi quia quis laudantium. Aut enim cupiditate totam sequi. Tempora occaecati labore modi.', 1),
(45, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'sit maxime', 49.6, 'Cum quia perferendis voluptatem vel. Distinctio voluptas omnis molestiae asperiores. Vero qui consequatur et sapiente qui dignissimos iure.', 1),
(46, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'qui debitis', 43.63, 'Excepturi ut velit qui. Consequatur consectetur a consequatur. Ipsam qui voluptates accusantium recusandae. Similique doloribus nobis praesentium nihil voluptatem velit.', 1),
(47, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'deserunt ut', 23.35, 'Quia labore quia et sint aliquid eligendi aspernatur non. Ab hic deserunt et. Omnis commodi quaerat quisquam. Nobis earum perspiciatis qui omnis sit incidunt.', 1),
(48, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'dolorum doloremque', 26.21, 'Molestiae quasi quos quam et cupiditate placeat quis et. Ab enim et molestiae. Nobis neque suscipit cupiditate voluptatem provident minus ipsum. Doloremque consequatur sit animi sed explicabo quis molestiae.', 1),
(49, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'omnis iste', 45.53, 'Dolores quo quibusdam eos magni ipsa distinctio voluptas. Eum vero qui ad suscipit dolorem placeat. Aut sed nam tempore excepturi est dicta autem. Beatae vero nobis inventore et provident nemo.', 1),
(50, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'ut itaque', 6.62, 'Voluptas vel velit repudiandae atque. Aut quis at pariatur et cupiditate. Ipsum alias neque reiciendis omnis. Doloremque veniam ut voluptas fuga dolorum vel ducimus.', 1),
(51, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'et accusamus', 7.83, 'Eum sequi hic dolor et enim. Consectetur voluptates facilis odit fugiat omnis. Nostrum et placeat culpa nulla eos. Rem iusto sint et recusandae atque hic. Voluptatum ipsum voluptas et saepe corrupti itaque.', 1),
(52, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'est omnis', 42.69, 'Aut ut consequatur officiis illo omnis accusantium. Maxime enim explicabo aut nobis. Hic excepturi delectus et architecto tempora ex laboriosam.', 1),
(53, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'beatae sit', 43.91, 'Similique illum asperiores exercitationem voluptatem voluptates assumenda excepturi. Ea reprehenderit facere ex eum sed accusamus rerum. Vitae quaerat et aliquid autem nemo velit asperiores nemo. Error et est dolor eveniet dolorum.', 1),
(54, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'neque in', 0.93, 'Est sed minima repellendus laboriosam iste. Voluptatem cum voluptatem rem sint quisquam dolores. Consequatur error eum incidunt omnis omnis voluptas laboriosam.', 1),
(55, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'est dolorem', 2.95, 'Perferendis tenetur natus molestiae. Culpa dolore in quibusdam distinctio velit. Harum dolorem et molestiae quam numquam magnam.', 1),
(56, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'ratione delectus', 43.65, 'At expedita est minima qui ipsa magni perferendis. Et ipsam et facere assumenda qui qui dolores id. Et et maxime explicabo aut impedit quis. Voluptates quia ab dolor impedit alias.', 1),
(57, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'omnis animi', 20.04, 'Laborum aut aperiam quidem eos et accusantium. Ut nobis hic libero iure rerum ea in accusamus. Omnis quia quia consequatur cumque aut doloribus.', 1),
(58, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'dignissimos et', 1.29, 'Et molestiae nihil veritatis quisquam quo est aut. Natus voluptatum qui mollitia tempora sed necessitatibus. Hic vitae expedita at. Quas hic ad ut aspernatur error ut repellendus.', 1),
(59, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'error iure', 12.85, 'Veniam vitae qui alias deleniti impedit. Nulla assumenda ipsum quasi officiis ut id.', 1),
(60, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'non ex', 8.9, 'Et modi sequi cumque minus voluptatem temporibus ut. Nobis laudantium voluptatem et velit officia aut provident minus. Necessitatibus odit esse aut adipisci nobis id quae. Expedita nihil a sed doloremque iure corrupti excepturi. Quia placeat accusantium aspernatur et commodi et aliquid.', 1),
(61, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'qui autem', 25.69, 'Voluptatem quia et facilis delectus delectus quo a. Sed magni voluptatem error numquam ipsum dolorem illum. Et aspernatur et assumenda consequatur eos cum iste. Error illum aut voluptatem itaque nihil.', 1),
(62, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'id debitis', 3.29, 'Itaque ut labore aperiam quisquam dolor quam. Quisquam sed soluta et nulla omnis dignissimos sit. Ab ab omnis culpa perspiciatis a esse corrupti.', 1),
(63, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'sequi cum', 46.25, 'Ducimus dolorum porro quaerat consectetur facere. Vel architecto consequatur voluptatem dignissimos vel ad vel. Qui quia ipsa quibusdam nemo nobis repudiandae dolor non.', 1),
(64, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'similique velit', 26.35, 'Consectetur blanditiis velit sed at id. Repellendus eaque deserunt quia exercitationem tempore sed. Et vero quasi et.', 1),
(65, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'dolorum voluptatem', 30.63, 'Quo dolor nemo non quibusdam. Debitis non autem est sit iste. Laboriosam exercitationem sit laudantium sint laudantium explicabo facere.', 1),
(66, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'qui aut', 31.33, 'Quaerat quia et blanditiis voluptas non reprehenderit quas. Facilis cum sit quaerat molestiae. Quisquam vero enim est. Sequi similique sed magnam voluptatibus qui et dolorem. Expedita eos repudiandae ut corporis itaque animi.', 1),
(67, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'porro impedit', 17.5, 'Enim modi qui consequuntur enim corporis mollitia dolorem quo. Assumenda animi architecto rem.', 1),
(68, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'error commodi', 44.42, 'Perspiciatis quas tenetur aliquam. Aut et ut nihil quo quia distinctio.', 1),
(69, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'illo nihil', 41.86, 'Sit minima unde et ex ab. Nihil id ut consequatur in nemo. Est sed aut quis placeat rem voluptates.', 1),
(70, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'nihil eius', 15.24, 'Mollitia officiis totam et eligendi. Dolore sint quam aspernatur. Consequatur debitis maxime eum et soluta velit quisquam.', 1),
(71, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'harum expedita', 4.18, 'Error ipsa tempore culpa praesentium cupiditate est est. Consequuntur iusto placeat voluptas repellendus eius sit. Saepe nihil omnis occaecati ut necessitatibus et vitae.', 1),
(72, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'rerum et', 25.82, 'Unde ut nihil unde doloremque odio et quibusdam. Reiciendis consectetur facere ipsam at. Harum distinctio quis qui et occaecati possimus.', 1),
(73, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'labore quo', 30.5, 'Officia temporibus velit aperiam in sunt. Explicabo consequuntur et voluptatibus temporibus quam nobis. Nesciunt laudantium eaque autem.', 1),
(74, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'culpa tenetur', 33, 'Fugit et eum est repellat qui. Aut aut deserunt consequatur debitis quis aut ab quia. Tempore qui labore recusandae magni commodi sed minus.', 1),
(75, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'omnis accusamus', 32.22, 'Et ipsa aliquid excepturi consectetur voluptate. Laboriosam magnam unde voluptatem numquam aut adipisci. Et aliquam et eos. Necessitatibus voluptatibus et asperiores.', 1),
(76, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'deleniti dolorem', 38.2, 'Officia consequatur maxime qui sint repellat. Deserunt repellendus quo possimus non vel molestias aliquid. Sunt assumenda sint quae qui veritatis provident ullam. Deleniti expedita ea sunt sit dicta.', 1),
(77, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'vitae delectus', 42.93, 'Minus enim magni iure non est unde suscipit eius. Ullam odit nostrum sunt sed labore laboriosam. Earum illum sint voluptas ullam. Neque qui dicta similique non. Est et quidem fugiat autem nam quidem iusto voluptate.', 1),
(78, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'molestiae esse', 5.55, 'Provident sunt ipsa rerum numquam omnis officiis. Possimus dolore eos et ullam qui nihil. Qui itaque maiores similique ut numquam. Quaerat sint possimus ipsam tempore commodi quaerat.', 1),
(79, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'praesentium neque', 35.03, 'Voluptatibus rerum inventore odit nam numquam voluptatem corporis. Delectus quidem cumque assumenda est iure deserunt labore doloribus. Maiores pariatur et excepturi omnis.', 1),
(80, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'est incidunt', 29.59, 'Officia architecto exercitationem perspiciatis nisi vitae rem similique. Veniam dolor aut iste non sunt iure occaecati sit. Odio beatae quia consequuntur velit ut quia harum enim. At laboriosam eligendi qui vel voluptas dolores non.', 1),
(81, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'dolores eos', 45.08, 'Possimus animi repudiandae tempore est. Consequatur aliquid doloribus minus velit rem. Qui beatae odit doloribus.', 1),
(82, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'sequi ipsum', 36.77, 'Quos quo magni ut autem. Qui animi fugiat rerum repudiandae soluta et. Ut quo harum tempora nam omnis. Et provident vero recusandae.', 1),
(83, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'blanditiis sequi', 23.52, 'Voluptas ipsam dolor nulla cupiditate suscipit. Minus magni qui autem maxime ad. Sapiente omnis odit pariatur nostrum. Dolores culpa eum placeat porro inventore.', 1),
(84, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'voluptatem adipisci', 14.53, 'Ad facere accusantium impedit et impedit a. Omnis assumenda et aperiam porro necessitatibus ipsam aspernatur. Iusto non aut quis temporibus recusandae nam tempora maxime.', 1),
(85, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'illo quos', 11.27, 'Aspernatur qui iusto consequatur sequi in nihil. Porro itaque sunt quos deserunt. Rerum dolor est commodi consequatur.', 1),
(86, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'quis nemo', 28.69, 'Et atque dolorem exercitationem velit ducimus. Quis cumque velit odit sed. Voluptas quisquam commodi non architecto.', 1),
(87, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'excepturi laudantium', 4.33, 'Voluptas quae itaque non sequi quia. Quae doloribus voluptate molestias repellat in aut. Architecto exercitationem ab voluptatem et harum sunt consectetur. Id est adipisci exercitationem odit ut.', 1),
(88, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'officia est', 42.04, 'Et ea omnis et laborum. Vel quibusdam eveniet ullam nobis et eligendi. Quam tenetur voluptatem aperiam porro et vel illum. Commodi magnam cum similique minima quo aliquam nesciunt.', 1),
(89, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'atque qui', 22.17, 'Et distinctio tenetur labore ut doloribus. Laudantium omnis quis eius perspiciatis quia culpa iste ducimus. Nihil voluptates et doloremque saepe assumenda ea earum quibusdam. Perferendis et exercitationem ipsam sit ut dignissimos.', 1),
(90, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'quo illum', 34.17, 'Inventore ea doloribus modi voluptatem totam asperiores. Quaerat est quibusdam dolore cupiditate voluptas laudantium beatae repellat. Nobis repudiandae consequatur unde doloremque. Sit odit eligendi rerum voluptas nostrum corporis.', 1),
(91, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'cumque et', 11.05, 'Accusamus voluptatibus harum qui dolore. Rerum eum ducimus aut vero harum distinctio.', 1),
(92, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'iusto voluptatem', 6.82, 'Inventore atque eum quis deserunt quo quis. Rerum recusandae architecto totam consequatur. Aut ex et sint deserunt et.', 1),
(93, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'sunt aspernatur', 4.04, 'Quas recusandae porro eveniet enim. Ipsum voluptatibus vel ab eius. Quaerat et aperiam quo tempore perspiciatis officiis. Et aperiam maiores nihil quis dignissimos.', 1),
(94, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'saepe esse', 30.84, 'Est nemo sunt natus libero distinctio et. Ut at hic quis.', 1),
(95, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'ipsa fuga', 17.9, 'Corrupti sit maxime sit cumque. Inventore sit quia sed ut sit consequatur. Rerum quis est ex quasi inventore.', 1),
(96, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'facere nihil', 4.05, 'Commodi quod quia ducimus molestiae. Quam architecto quo est nihil enim. Ut ut libero ut vitae ab qui expedita. Et laborum quia blanditiis quos.', 1),
(97, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'et ullam', 36.09, 'Et maxime delectus sint. Excepturi et sit similique dicta. Similique in qui id possimus beatae voluptatem.', 1),
(98, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'labore assumenda', 41.87, 'Animi et non consequatur rerum asperiores qui. A magnam voluptates quis inventore eligendi et. At ea esse nisi nam.', 1),
(99, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'excepturi velit', 26.57, 'Voluptatum et earum velit omnis error accusantium porro. Quod harum pariatur animi doloremque. Consectetur id eum enim necessitatibus minus.', 1),
(100, '2020-01-13 03:20:13', '2020-01-13 03:20:13', 'hic iure', 32.69, 'Delectus maxime quas explicabo aut in qui assumenda sequi. Atque quaerat nihil eos similique. Sequi sed quis vel. Doloribus amet cum placeat in quis distinctio eius.', 1);

-- --------------------------------------------------------

--
-- Struktura tabele `ratings`
--

CREATE TABLE `ratings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `value` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabele `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Odloži podatke za tabelo `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Stranka', '2020-01-11 13:22:59', '2020-01-11 13:22:59'),
(2, 'Prodajalec', '2020-01-11 13:22:59', '2020-01-11 13:22:59'),
(3, 'Administrator', '2020-01-11 13:22:59', '2020-01-11 13:22:59');

-- --------------------------------------------------------

--
-- Struktura tabele `shopping_carts`
--

CREATE TABLE `shopping_carts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Odloži podatke za tabelo `shopping_carts`
--

INSERT INTO `shopping_carts` (`id`, `created_at`, `updated_at`, `user_id`) VALUES
(1, '2020-01-13 03:20:06', '2020-01-13 03:20:06', 1),
(2, '2020-01-13 03:20:06', '2020-01-13 03:20:06', 2),
(3, '2020-01-13 03:20:06', '2020-01-13 03:20:06', 3),
(4, '2020-01-13 03:20:06', '2020-01-13 03:20:06', 4),
(5, '2020-01-13 03:20:06', '2020-01-13 03:20:06', 5),
(6, '2020-01-13 03:20:06', '2020-01-13 03:20:06', 6),
(7, '2020-01-13 03:20:06', '2020-01-13 03:20:06', 7),
(8, '2020-01-13 03:20:06', '2020-01-13 03:20:06', 8),
(9, '2020-01-13 03:20:06', '2020-01-13 03:20:06', 9),
(10, '2020-01-13 03:20:06', '2020-01-13 03:20:06', 10),
(11, '2020-01-13 03:20:06', '2020-01-13 03:20:06', 11),
(12, '2020-01-13 03:20:07', '2020-01-13 03:20:07', 12),
(13, '2020-01-13 03:20:07', '2020-01-13 03:20:07', 13),
(14, '2020-01-13 03:20:07', '2020-01-13 03:20:07', 14),
(15, '2020-01-13 03:20:07', '2020-01-13 03:20:07', 15),
(16, '2020-01-13 03:20:07', '2020-01-13 03:20:07', 16),
(17, '2020-01-13 03:20:07', '2020-01-13 03:20:07', 17),
(18, '2020-01-13 03:20:07', '2020-01-13 03:20:07', 18),
(19, '2020-01-13 03:20:07', '2020-01-13 03:20:07', 19),
(20, '2020-01-13 03:20:07', '2020-01-13 03:20:07', 20),
(21, '2020-01-13 03:20:07', '2020-01-13 03:20:07', 21),
(22, '2020-01-13 03:20:07', '2020-01-13 03:20:07', 22),
(23, '2020-01-13 03:20:07', '2020-01-13 03:20:07', 23),
(24, '2020-01-13 03:20:07', '2020-01-13 03:20:07', 24),
(25, '2020-01-13 03:20:07', '2020-01-13 03:20:07', 25),
(26, '2020-01-13 03:20:07', '2020-01-13 03:20:07', 26),
(27, '2020-01-13 03:20:07', '2020-01-13 03:20:07', 27),
(28, '2020-01-13 03:20:08', '2020-01-13 03:20:08', 28),
(29, '2020-01-13 03:20:08', '2020-01-13 03:20:08', 30),
(30, '2020-01-13 03:20:08', '2020-01-13 03:20:08', 31),
(31, '2020-01-13 03:20:08', '2020-01-13 03:20:08', 32),
(32, '2020-01-13 03:20:08', '2020-01-13 03:20:08', 34),
(33, '2020-01-13 03:20:08', '2020-01-13 03:20:08', 35),
(34, '2020-01-13 03:20:08', '2020-01-13 03:20:08', 36),
(35, '2020-01-13 03:20:08', '2020-01-13 03:20:08', 37),
(36, '2020-01-13 03:20:08', '2020-01-13 03:20:08', 38),
(37, '2020-01-13 03:20:08', '2020-01-13 03:20:08', 39),
(38, '2020-01-13 03:20:08', '2020-01-13 03:20:08', 40),
(39, '2020-01-13 03:20:08', '2020-01-13 03:20:08', 41),
(40, '2020-01-13 03:20:08', '2020-01-13 03:20:08', 42),
(41, '2020-01-13 03:20:09', '2020-01-13 03:20:09', 43),
(42, '2020-01-13 03:20:09', '2020-01-13 03:20:09', 44),
(43, '2020-01-13 03:20:09', '2020-01-13 03:20:09', 45),
(44, '2020-01-13 03:20:09', '2020-01-13 03:20:09', 46);

-- --------------------------------------------------------

--
-- Struktura tabele `shopping_cart_items`
--

CREATE TABLE `shopping_cart_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabele `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `cart_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Odloži podatke za tabelo `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `api_token`, `remember_token`, `created_at`, `updated_at`, `phone`, `active`, `cart_id`, `role_id`) VALUES
(1, 'Borut Krajnc', 'borut.krajnc@ep.si', NULL, '$2y$10$4775LP5F52Lf6h.53xznW.z0jSNpocIIjfd1wxlz8OLyAh5lvMNmu', NULL, NULL, '2020-01-13 03:20:06', '2020-01-13 03:20:06', '069421275', 1, 1, 1),
(2, 'Branimir Horvat', 'branimir.horvat@ep.si', NULL, '$2y$10$hXVHC./f56gg2mggNUfOu.p06fxRSWW6/uL4Lgmz0ogQwxIh1F/e6', NULL, NULL, '2020-01-13 03:20:06', '2020-01-13 03:20:06', '070944671', 1, 2, 1),
(3, 'Benebod Korošec', 'benebod.korosec@ep.si', NULL, '$2y$10$j9yMcIfdoxG5igQtUumBTe9X4zQK6Hk3EHSI7gjMKq9zcwpo59QIS', NULL, NULL, '2020-01-13 03:20:06', '2020-01-13 03:20:06', '070547319', 1, 3, 1),
(4, 'Borut Mlakar', 'borut.mlakar@ep.si', NULL, '$2y$10$/BxQTDiXslIMpuZRjmfIceRDSrNV3Kr8zFNlB8tzzyzGHHMySAXBq', NULL, NULL, '2020-01-13 03:20:06', '2020-01-13 03:20:06', '070832485', 1, 4, 1),
(5, 'Bor Vidmar', 'bor.vidmar@ep.si', NULL, '$2y$10$0mGLlD6fd/15ztTX7cQTCu7qkmPSddPkJqBpuwEuTlFvWsHeJ./A.', NULL, NULL, '2020-01-13 03:20:06', '2020-01-13 03:20:06', '031905862', 1, 5, 1),
(6, 'Andraž Krajnc', 'andraz.krajnc@ep.si', NULL, '$2y$10$ni7pvfPX1eI7WxC2.vDtouHFinmtfcAk2JaOM.wzSMBIRmoCSHs9q', NULL, NULL, '2020-01-13 03:20:06', '2020-01-13 03:20:06', '031424060', 1, 6, 1),
(7, 'Benebod Kovačič', 'benebod.kovacic@ep.si', NULL, '$2y$10$tsD/fKkORU4NlR6/VQe7y.yBGnXmnRNHMHcsVuckS8PXZ8LKatIHG', NULL, NULL, '2020-01-13 03:20:06', '2020-01-13 03:20:06', '031485487', 1, 7, 1),
(8, 'Borin Korošec', 'borin.korosec@ep.si', NULL, '$2y$10$AmSzhkICKEdp1Fy77dpoNOrdur.n17vXW1XmrLdLMqCtMRUtT7DBu', NULL, NULL, '2020-01-13 03:20:06', '2020-01-13 03:20:06', '051030492', 1, 8, 1),
(9, 'Borin Kovač', 'borin.kovac@ep.si', NULL, '$2y$10$6rZTXwaUhuAwvsoRFkWLz.LJZSIy7nNOzFBk7ldIrhvmlNwBmCS7u', NULL, NULL, '2020-01-13 03:20:06', '2020-01-13 03:20:06', '031253087', 1, 9, 1),
(10, 'Belot Zupančič', 'belot.zupancic@ep.si', NULL, '$2y$10$S7l4r7WJBmCXDW3oy8.5KuRIFUUXC5GDNkKqRe9eG/A0sKk4lCNTK', NULL, NULL, '2020-01-13 03:20:06', '2020-01-13 03:20:06', '069592496', 1, 10, 1),
(11, 'Borižit Zupančič', 'borizit.zupancic@ep.si', NULL, '$2y$10$rfj70Q9kopYibjEg0HVW7upjDJk6GJ9FYtXAN9ZvLjYjEEO0OWIGS', NULL, NULL, '2020-01-13 03:20:06', '2020-01-13 03:20:06', '041045697', 1, 11, 1),
(12, 'Boran Kovačič', 'boran.kovacic@ep.si', NULL, '$2y$10$3Sth.z/okD9j0k/kKIaoTOEHNZRFw1Yq4mzj4qc0k/Bx/.jfI/3vS', NULL, NULL, '2020-01-13 03:20:07', '2020-01-13 03:20:07', '069194170', 1, 12, 1),
(13, 'Belot Novak', 'belot.novak@ep.si', NULL, '$2y$10$3H5TckR4Ja9ilS9PjgqBB.V0aB5S0Mzc1Uxdge7SUtom7k.R.Djga', NULL, NULL, '2020-01-13 03:20:07', '2020-01-13 03:20:07', '041568362', 1, 13, 1),
(14, 'Anže Krajnc', 'anze.krajnc@ep.si', NULL, '$2y$10$rvviWPA8IL6HBWzUH4IiXe.6QTlG7j25X.jrQPKtA3KBXzHRa0ojW', NULL, NULL, '2020-01-13 03:20:07', '2020-01-13 03:20:07', '051834993', 1, 14, 1),
(15, 'Božjak Božič', 'bozjak.bozic@ep.si', NULL, '$2y$10$J6EVDqLgNvUFEs0AUaMCDuT35lR2Rr9YAScFwQ9fKXI/v0KTR/8vC', NULL, NULL, '2020-01-13 03:20:07', '2020-01-13 03:20:07', '041187696', 1, 15, 1),
(16, 'Bogomir Krajnc', 'bogomir.krajnc@ep.si', NULL, '$2y$10$Kq222hyX.W9nwqFeHDpvZ.HFvhdXJv1u0pZYzdl31GfhIVrcftTiO', NULL, NULL, '2020-01-13 03:20:07', '2020-01-13 03:20:07', '070978767', 1, 16, 1),
(17, 'Belot Mlakar', 'belot.mlakar@ep.si', NULL, '$2y$10$Ja2MEdtzU8lX7ke.5rJdVukUu.2DOADUMp8fJSzdhNnnlNl8Q.hQu', NULL, NULL, '2020-01-13 03:20:07', '2020-01-13 03:20:07', '041536669', 1, 17, 1),
(18, 'Benebod Kos', 'benebod.kos@ep.si', NULL, '$2y$10$VWMf7WrqY8lSOyWP9MaPDuwtVuf9N.fEObEoczDEe0Zuqst5.jh6e', NULL, NULL, '2020-01-13 03:20:07', '2020-01-13 03:20:07', '069245355', 1, 18, 1),
(19, 'Anže Vidmar', 'anze.vidmar@ep.si', NULL, '$2y$10$BVn2wr.XemY2mFtQkWjtEOIUrT/5/FAuH461afiLaTSd/U8jQb7US', NULL, NULL, '2020-01-13 03:20:07', '2020-01-13 03:20:07', '031863568', 1, 19, 1),
(20, 'Aljaž Kovač', 'aljaz.kovac@ep.si', NULL, '$2y$10$4DLa3V58HGyvboAInndUa.howJhqUHiIYvzGcPIJWGwEbv5Kf0JzC', NULL, NULL, '2020-01-13 03:20:07', '2020-01-13 03:20:07', '051636676', 1, 20, 1),
(21, 'Božjak Potočnik', 'bozjak.potocnik@ep.si', NULL, '$2y$10$gk.AanIPqbMzsiV9IWYEAusFXceBl2kYe0JXPmlPa9YaD4EJshgJi', NULL, NULL, '2020-01-13 03:20:07', '2020-01-13 03:20:07', '031725018', 1, 21, 1),
(22, 'Borin Kralj', 'borin.kralj@ep.si', NULL, '$2y$10$KpmntGrxaH.YWt5aNU/qcesFYmT8PSIrtZ/n3GVTUxZ/1vZHt6iVW', NULL, NULL, '2020-01-13 03:20:07', '2020-01-13 03:20:07', '041020392', 1, 22, 1),
(23, 'Andraž Mlakar', 'andraz.mlakar@ep.si', NULL, '$2y$10$7bDcMY3iVIyJKdpqgV7WsOud92Y4DArwt19QXJVAcMXtSCcJZeGFe', NULL, NULL, '2020-01-13 03:20:07', '2020-01-13 03:20:07', '031094642', 1, 23, 1),
(24, 'Andraž Vidmar', 'andraz.vidmar@ep.si', NULL, '$2y$10$V4Cw3zZZa3nRYvaOhFjUzOISGXlJJeJo2CmFu0p6WsQBmEKtfUWni', NULL, NULL, '2020-01-13 03:20:07', '2020-01-13 03:20:07', '031159317', 1, 24, 1),
(25, 'Boran Zupančič', 'boran.zupancic@ep.si', NULL, '$2y$10$y9.hQ0fvg61LvoPCsp2DPOf82TGUCLHjFRaQ3K5JZAizE4067klZ2', NULL, NULL, '2020-01-13 03:20:07', '2020-01-13 03:20:07', '070307730', 1, 25, 1),
(26, 'Belot Kovačič', 'belot.kovacic@ep.si', NULL, '$2y$10$y.8zJorkNyi1erbK4QF7NesSVkX/T7f30gU2tdlOliWVHtwto96FO', NULL, NULL, '2020-01-13 03:20:07', '2020-01-13 03:20:07', '041035478', 1, 26, 1),
(27, 'Božjak Mlakar', 'bozjak.mlakar@ep.si', NULL, '$2y$10$//XqAxURw/5yPjjXgIhWSOIno77xjCLN8MKpLp/pZC8HqIrnNWgh.', NULL, NULL, '2020-01-13 03:20:07', '2020-01-13 03:20:07', '070081027', 1, 27, 1),
(28, 'Božjak Kos', 'bozjak.kos@ep.si', NULL, '$2y$10$Al0f7h0MjkdBaW5aElpRquLZgwcF/9471h.ef.KcWtXZ0qr1qwJUK', NULL, NULL, '2020-01-13 03:20:08', '2020-01-13 03:20:08', '070264227', 1, 28, 1),
(30, 'Bogomir Kos', 'bogomir.kos@ep.si', NULL, '$2y$10$Tk0SWsQoZ3M9OxMj61BhauBec0wCYhPbikzoKWvN40h7rr3OMA5va', NULL, NULL, '2020-01-13 03:20:08', '2020-01-13 03:20:08', '070537982', 1, 29, 1),
(31, 'Bogomir Vidmar', 'bogomir.vidmar@ep.si', NULL, '$2y$10$3XifuUbOXRW/628YN9HMGuEzEBpFZwPd4gzV5oz2XRg.7oXFz2Lpq', NULL, NULL, '2020-01-13 03:20:08', '2020-01-13 03:20:08', '031462278', 1, 30, 2),
(32, 'Aljaž Mlakar', 'aljaz.mlakar@ep.si', NULL, '$2y$10$0BTGDTA4LiTQSy1BdjnBjubRos2lxOQoyu3sbwoUiI5zj4BZ.7MB6', NULL, NULL, '2020-01-13 03:20:08', '2020-01-13 03:20:08', '069931912', 1, 31, 2),
(34, 'Anže Potočnik', 'anze.potocnik@ep.si', NULL, '$2y$10$0MG8.ALS62phXFjVYY1MJeldjZqix4zKlzUwfSKYnDNv/iAVIl566', NULL, NULL, '2020-01-13 03:20:08', '2020-01-13 03:20:08', '031006992', 1, 32, 2),
(35, 'Bogomir Korošec', 'bogomir.korosec@ep.si', NULL, '$2y$10$9U37gvNUhGz4zS/AwX1xF.EHxpUcqt7Y8Q6pamAkI.oErTmQ71Nz6', NULL, NULL, '2020-01-13 03:20:08', '2020-01-13 03:20:08', '051813523', 1, 33, 2),
(36, 'Benebod Vidmar', 'benebod.vidmar@ep.si', NULL, '$2y$10$TiekglXGR8iJgUhlM0un3O1SkUrgvkyE7DDuun8FF40mpKvOPPQPa', NULL, NULL, '2020-01-13 03:20:08', '2020-01-13 03:20:08', '051408164', 1, 34, 2),
(37, 'Bogomir Potočnik', 'bogomir.potocnik@ep.si', NULL, '$2y$10$p4Yb6rzuuxTGz2j0Wh5Db.PU12WfNUtBTEtihRVmS0eUXCFkHbdDC', NULL, NULL, '2020-01-13 03:20:08', '2020-01-13 03:20:08', '041854013', 1, 35, 2),
(38, 'Andraž Kovač', 'andraz.kovac@ep.si', NULL, '$2y$10$wG3y5A/NDOdKsV6LbrT1Hu7pTRZ6BowzdJrBJoaXnVNjUawIUqv1C', NULL, NULL, '2020-01-13 03:20:08', '2020-01-13 03:20:08', '041625335', 1, 36, 2),
(39, 'Belot Horvat', 'belot.horvat@ep.si', NULL, '$2y$10$SK.P/CV9J9G7zsAv7INnMetQ2KGAqfHXCfGo09Lx83tMpY4Fyb6hi', NULL, NULL, '2020-01-13 03:20:08', '2020-01-13 03:20:08', '051490658', 1, 37, 2),
(40, 'Bogomir Zupančič', 'bogomir.zupancic@ep.si', NULL, '$2y$10$twOJCG79Db4wkTJtDkpa..SyWZKt01/nlwn0eo3E11E.F25.Mq7Hu', NULL, NULL, '2020-01-13 03:20:08', '2020-01-13 03:20:08', '051580502', 1, 38, 2),
(41, 'Bor Kralj', 'bor.kralj@ep.si', NULL, '$2y$10$ErsgOdywvuPdVqbBIccFVuGXKHe6JdFe.gHwz0Xn.V923/.QKi3gK', NULL, NULL, '2020-01-13 03:20:08', '2020-01-13 03:20:08', '041981121', 1, 39, 2),
(42, 'Bogo Golob', 'bogo.golob@ep.si', NULL, '$2y$10$dHY4Ij5huik.SFxClKhrB.3OlB7usDUCcrBYq.mzYVHHqgvVI.dbi', NULL, NULL, '2020-01-13 03:20:08', '2020-01-13 03:20:08', '069825034', 1, 40, 2),
(43, 'Luka Galjot', 'luka@ep.si', NULL, '$2y$10$iq9vTNwAivHfY1YSBwCa2eoNxh7egjahqBdKpTWx37TjTFF2gI6J2', NULL, NULL, '2020-01-13 03:20:09', '2020-01-13 03:20:09', '10963944', 1, 41, 3),
(44, 'Jana Štremfelj', 'jana@ep.si', NULL, '$2y$10$2qa3dbaB2L6D7nvYl6tNuOK70ZuAlvYKvOWidvp4LCqqC60zkdiYW', NULL, NULL, '2020-01-13 03:20:09', '2020-01-13 03:20:09', '10965588', 1, 42, 2),
(45, 'Rok Peterlin', 'rok@ep.si', NULL, '$2y$10$YSt.UPJpNeBGuPBL/M1fI.KSProAz2R11zKGKn/D94cUOz81dv13.', NULL, NULL, '2020-01-13 03:20:09', '2020-01-13 03:20:09', '10995356', 1, 43, 2),
(46, 'David Jelenc', 'david@ep.si', NULL, '$2y$10$srQF7GlkqC/gfCaqsCeoL.02VuhyHkw1jHGkp43QvjqVDajf5r6r6', NULL, NULL, '2020-01-13 03:20:09', '2020-01-13 03:20:09', '10991964', 1, 44, 1);

-- --------------------------------------------------------

--
-- Struktura tabele `user_roles`
--

CREATE TABLE `user_roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Odloži podatke za tabelo `user_roles`
--

INSERT INTO `user_roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Stranka', '2020-01-13 03:20:06', '2020-01-13 03:20:06'),
(2, 'Prodajalec', '2020-01-13 03:20:06', '2020-01-13 03:20:06'),
(3, 'Administrator', '2020-01-13 03:20:06', '2020-01-13 03:20:06');

--
-- Indeksi zavrženih tabel
--

--
-- Indeksi tabele `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeksi tabele `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indeksi tabele `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeksi tabele `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indeksi tabele `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indeksi tabele `order_statuses`
--
ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indeksi tabele `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeksi tabele `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `products` ADD FULLTEXT KEY `fulltext_index` (`name`);

--
-- Indeksi tabele `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indeksi tabele `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indeksi tabele `shopping_carts`
--
ALTER TABLE `shopping_carts`
  ADD PRIMARY KEY (`id`);

--
-- Indeksi tabele `shopping_cart_items`
--
ALTER TABLE `shopping_cart_items`
  ADD PRIMARY KEY (`id`);

--
-- Indeksi tabele `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_api_token_unique` (`api_token`);

--
-- Indeksi tabele `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT zavrženih tabel
--

--
-- AUTO_INCREMENT tabele `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT tabele `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT tabele `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=506;

--
-- AUTO_INCREMENT tabele `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT tabele `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT tabele `order_statuses`
--
ALTER TABLE `order_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT tabele `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT tabele `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT tabele `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT tabele `shopping_carts`
--
ALTER TABLE `shopping_carts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT tabele `shopping_cart_items`
--
ALTER TABLE `shopping_cart_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT tabele `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT tabele `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
